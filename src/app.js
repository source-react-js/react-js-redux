import './app.css'
import  { TodoListHook, TodoListClass } from './Container'
export const App = () => {
  return (
    <div className="app">
      <TodoListHook/>
      <TodoListClass/>
    </div>
  )
}
