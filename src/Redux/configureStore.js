import thunkMiddleware from 'redux-thunk' //For watch log action redux
import { createStore, applyMiddleware } from 'redux'
import combineReducers from './combineReducers'
export default createStore(combineReducers, applyMiddleware(thunkMiddleware))