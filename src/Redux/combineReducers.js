import { combineReducers } from 'redux'
import todoListReducer  from './TodoList/reducer'
export default combineReducers({
    todoListReducer,
  })
  