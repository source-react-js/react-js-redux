import { ADD_DATA, DELETE_DATA} from './actionTypes'

export const addData = (text) => {
  return {
    type: ADD_DATA,
    text
  }
}

export const deleteData = (index) => {
  return {
    type: DELETE_DATA,
    index
  }
}