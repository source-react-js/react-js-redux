import React, { Component } from "react"
import { TodoListComponent } from '../../Component';
import { TODO_LIST } from '../../Redux/index'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'

class TodoList extends Component {
  constructor() {
    super()
    this.state = {
      text: '',
    }
  }

  clickAdd = () => {
    const { text } = this.state
    const { todoListAction } = this.props
    if (text) {
      this.setState({
        text: ''
      }, () => {
        todoListAction.addData(text)
      })
    }
  }

  clickDelete = (index) => {
    const { todoListAction } = this.props
    todoListAction.deleteData(index)
  }

  changeText = (element) => {
    if (element.key === "Enter") {
      this.setState({
        text: element.target.value
      }, () => {
        this.clickAdd()
      })
    } else {
      this.setState({
        text: element.target.value
      })
    }
  }

  render() {
    const { text } = this.state
    const { todoListReducer: { list } } = this.props
    return (
      <TodoListComponent
        text={text}
        list={list}
        changeText={this.changeText}
        clickAdd={this.clickAdd}
        clickDelete={this.clickDelete}
        disabled={list.length === 10}
      />
    )
  }
}
const mapDispatchToProps = dispatch => ({
  todoListAction: bindActionCreators(TODO_LIST.actionCreators, dispatch),
})

export const mapStateToProps = ({ todoListReducer, }) => {
  return {
    todoListReducer
  }
}

export const TodoListClass = (connect(mapStateToProps, mapDispatchToProps)(TodoList))
