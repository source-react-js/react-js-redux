import { useState } from "react"
import { useDispatch, useSelector } from "react-redux"
import { TODO_LIST } from '../../Redux/index'
import { TodoListComponent } from '../../Component'

const { actionCreators: { addData, deleteData } } = TODO_LIST

export const TodoListHook = () => {
  const { list } = useSelector(({ todoListReducer }) => todoListReducer)
  const [text, setText] = useState('')
  const dispatch = useDispatch()

  const clickAdd = () => {
    if (text) {
      setText('')
      dispatch(addData(text))
    }
  }

  const clickDelete = (index) => {
    dispatch(deleteData(index))
  }

  const changeText = ({ key, target }) => {
    key === "Enter" ? clickAdd() : setText(target.value)
  }

  return (
    <TodoListComponent
      text={text}
      list={list}
      changeText={changeText}
      clickAdd={clickAdd}
      clickDelete={clickDelete}
      disabled={list.length === 10}
    />
  )
}